<?php
/**
 * Created by PhpStorm.
 * User: tn
 * Date: 05/05/2020
 * Time: 16:29
 */

namespace NobrainerWeb\TrackMemberLogin;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordViewer;
use SilverStripe\Security\Security;
use SilverStripe\ORM\DataExtension;

/**
 * Class TrackMemberExtension
 *
 * Shamelessly stolen from https://docs.silverstripe.org/en/4/developer_guides/extending/how_tos/track_member_logins/
 *
 * @package nobrainerweb\TrackMemberLogin
 */
class TrackMemberLoginExtension extends DataExtension
{
    private static $db = [];

    private static $has_many = [
        'MemberLogins' => MemberLogin::class,
    ];

    private static $cascade_deletes = [
        'MemberLogins',
    ];

    public function afterMemberLoggedIn()
    {
        $this->logMemberLogin();
    }

    /* TODO: is this deprecated?
    public function memberAutoLoggedIn()
    {
        $this->logMemberLogin();
    }
    */

    public function updateCMSFields(FieldList $fields)
    {
        // Relation handler for MemberLogins
        $fields->removeByName(['MemberLogins']);
        $login_config = GridFieldConfig_RecordViewer::create(20);
        $login_gridfield = GridField::create('MemberLogins', 'MemberLogins', $this->owner->MemberLogins(),
            $login_config);

        $fields->addFieldsToTab('Root.Logins', [
            $login_gridfield
        ]);

    }

    protected function logMemberLogin()
    {

        if (!Security::database_is_ready()) {
            return;
        }

        $login = MemberLogin::create();
        $login->IP = $this->GetVisitorIP();
        $login->write();
        $this->owner->MemberLogins()->Add($login);

    }

    /**
     * Function returns visitors IP address
     *
     */
    protected function GetVisitorIP()
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip_address = $_SERVER['REMOTE_ADDR'];
        }

        return trim($ip_address);
    }
}
