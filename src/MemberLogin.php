<?php

namespace NobrainerWeb\TrackMemberLogin;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\ReadonlyField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Security\Member;

/**
 *
 * Class _DataObjectStencil
 * @package NobrainerWeb\App\DataObjects
 */
class MemberLogin extends DataObject
{
    // Naming definitions for the model.
    private static $table_name = 'NW_MemberLogin';
    private static $singular_name = 'Member login';
    private static $plural_name = 'Member logins';

    private static $db = [
        'IP' => 'Varchar'
    ];

    private static $has_one = [
        'Member' => Member::class
    ];

    /**
     * @var string|null
     */
    private static $default_sort = 'Created DESC';

    /**
     *
     * @var array
     */
    private static $summary_fields = [
        'Created.Nice' => 'Time',
        'Member.Name'  => 'Member',
        'IP'           => 'IP'
    ];

    /**
     * Get the fields from the CMS that are necessary to fill data into the model.
     * Using $fields->addFieldsToTab() reduces the need for rewriting code
     * when more fields must be added to the given tab.
     *
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->addFieldsToTab('Root.Main', [
            ReadonlyField::create('Created'),
        ]);

        return $fields;
    }

}